package com.gitlab;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@SpringBootApplication
@Configuration
@EntityScan(basePackages = {"com.gitlab"})
public class App {

    public static void main(String[] args) throws Exception {
          new SpringApplicationBuilder(App.class).run();


    }


}


